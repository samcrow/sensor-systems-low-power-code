
#include <msp430.h>				


void main() {
	// Disable watchdog timer
	WDTCTL = WDTPW | WDTHOLD;

	//P1.0 output mode and on
	P1DIR |= BIT0;
	P1OUT |= BIT0;

	// All other pins to output mode
	P1DIR |= 0xFF;
	P2DIR |= 0xFF;
	P3DIR |= 0xFF;
	P4DIR |= 0xFF;

	// P2 and P3 all off
	P2OUT = 0;
	P3OUT = 0;

	int i;
	for(i = 10; i > 0; i--) {

		// Toggle P1.0
		P1OUT ^= BIT0;

		// Wait
		volatile unsigned int t = 10000;
		while(t > 0) {
			t--;
		}

	}
	// P1.0 off
	P1OUT &= ~BIT0;


	// Enter low-power mode 4
	_BIS_SR(LPM4_bits + GIE);
	
} // End of main()


