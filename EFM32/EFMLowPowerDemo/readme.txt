Select a single energy mode, and stay there.

This example project uses the EFM32 CMSIS and demonstrates the use of
the LCD display, RTC (real time counter), GPIO and various Energy
Modes (EM).

Use PB1 to cycle through the energy mode tests available.
Press PB0 to start selected test.

This demo application has been made to give a quick demo of the
energyAware Profiler from Silicon Labs, including EFM32 energy modes.

Board:  Silicon Labs EFM32ZG-STK3200 Development Kit
Device: EFM32ZG222F32

= Modified by Sam Crow =

The code was changed so that it would enter various low-power modes and then return to normal after a few seconds.
This worked with EM0-EM3.

However, upon entering EM4 (the lowest-power mode), the microcontroller became unresponsive. Nothing so far has successfully resuscitated it.
