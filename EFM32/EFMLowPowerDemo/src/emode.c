/**************************************************************************//**
 * @file
 * @brief Demo for energy mode current consumption testing.
 * @author Energy Micro AS
 * @version 3.20.3
 ******************************************************************************
 * @section License
 * <b>(C) Copyright 2014 Silicon Labs, http://www.silabs.com</b>
 *******************************************************************************
 *
 * This file is licensed under the Silicon Labs Software License Agreement. See 
 * "http://developer.silabs.com/legal/version/v11/Silicon_Labs_Software_License_Agreement.txt"  
 * for details. Before using this software for any purpose, you must agree to the 
 * terms of that agreement.
 *
 ******************************************************************************/


#include <stdio.h>

#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"
#include "em_rtc.h"

#include "display.h"
#include "textdisplay.h"
#include "retargettextdisplay.h"

#define NO_OF_EMODE_TESTS            (9) /* Number of energy modes.     */

#define RTC_CALLBACK_FREQUENCY      (64) /* Frequency of RTC callbacks. */

/* RTC callback parameters. */
static void (*rtcCallback)(void*) = NULL;
static void*  rtcCallbackArg      = 0;

static volatile int      eMode;          /* Selected energy mode.            */
static volatile bool     startTest;      /* Start selected energy mode test. */
static volatile bool     displayEnabled; /* Status of LCD display.           */
static volatile uint32_t seconds = 0;     /* Seconds elapsed since reset.    */
static volatile int      rtcIrqCount = 0; /* RTC interrupt counter           */
static DISPLAY_Device_t  displayDevice;  /* Display device handle.           */

static void GpioSetup(void);
static void RtcInit(void);
static void SelectClock( CMU_Select_TypeDef hfClockSelect,
                         uint32_t clockDisableMask );
static void EnterEMode( int mode, uint32_t secs );

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
int main(void)
{
  int currentEMode;

  /* Chip errata */
  CHIP_Init();

  /* Setup GPIO for pushbuttons. */
  GpioSetup();
  /* Set RTC to generate an interrupt every second. */
  RtcInit();
  /* Disable LFB clock tree. */
  CMU->LFCLKSEL &= ~(_CMU_LFCLKSEL_LFB_MASK);

  // Try EM3
  EnterEMode(3, 5);


  while(1) {}
}

/**************************************************************************//**
 * @brief   Enter and stay in Energy Mode for a given number of seconds.
 *
 * @param[in] mode  Energy Mode to enter (1..4).
 * @param[in] secs  Time to stay in Energy Mode <mode>.
 *****************************************************************************/
static void EnterEMode( int mode, uint32_t secs )
{
  if ( secs )
  {
    uint32_t startTime = seconds;

    while ((seconds - startTime) < secs)
    {
      switch ( mode )
      {
        case 1: EMU_EnterEM1();         break;
        case 2: EMU_EnterEM2( false );  break;
        case 3: EMU_EnterEM3( false );  break;
        case 4: EMU_EnterEM4();         break;
      default:
        /* Invalid mode. */
        while(1);
      }
    }
  }
}

/**************************************************************************//**
 * @brief Setup GPIO interrupt for pushbuttons.
 *****************************************************************************/
static void GpioSetup(void)
{
  /* Enable GPIO clock */
  CMU_ClockEnable(cmuClock_GPIO, true);

  /* Configure PC8 as input and enable interrupt  */
  GPIO_PinModeSet(gpioPortC, 8, gpioModeInputPull, 1);
  GPIO_IntConfig(gpioPortC, 8, false, true, true);

  NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
  NVIC_EnableIRQ(GPIO_EVEN_IRQn);

  /* Configure PC9 as input and enable interrupt */
  GPIO_PinModeSet(gpioPortC, 9, gpioModeInputPull, 1);
  GPIO_IntConfig( gpioPortC, 9, false, true, true);

  NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
  NVIC_EnableIRQ(GPIO_ODD_IRQn);
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB1)
 *        Starts selected energy mode test.
 *****************************************************************************/
void GPIO_ODD_IRQHandler(void)
{
  /* Acknowledge interrupt */
  GPIO_IntClear(1 << 9);

  eMode = (eMode + 1) % NO_OF_EMODE_TESTS;
}

/**************************************************************************//**
 * @brief GPIO Interrupt handler (PB0)
 *        Sets next energy mode test number.
 *****************************************************************************/
void GPIO_EVEN_IRQHandler(void)
{
  /* Acknowledge interrupt */
  GPIO_IntClear(1 << 8);

  startTest = true;
}

/**************************************************************************//**
 * @brief   Set up RTC to generate an interrupt every second.
 *****************************************************************************/
static void RtcInit(void)
{
  RTC_Init_TypeDef rtcInit = RTC_INIT_DEFAULT;

  /* Enable LE domain registers */
  CMU_ClockEnable(cmuClock_CORELE, true);

  /* Enable ULFRCO as LFACLK in CMU. */
  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_ULFRCO);

  /* Set the prescaler. */
  CMU_ClockDivSet(cmuClock_RTC, cmuClkDiv_1);

  /* Enable RTC clock */
  CMU_ClockEnable(cmuClock_RTC, true);

  /* Initialize RTC */
  rtcInit.enable   = false;  /* Do not start RTC after initialization is complete. */
  rtcInit.debugRun = false;  /* Halt RTC when debugging. */
  rtcInit.comp0Top = true;   /* Wrap around on COMP0 match. */
  RTC_Init(&rtcInit);

  /* Interrupt at specified frequency. */
  RTC_CompareSet(0, (SystemULFRCOClockGet() / RTC_CALLBACK_FREQUENCY) - 1);

  /* Enable interrupt */
  NVIC_EnableIRQ(RTC_IRQn);
  RTC_IntEnable(RTC_IEN_COMP0);

  /* Start counter */
  RTC_Enable(true);
}

/**************************************************************************//**
 * @brief   Register a callback function at the given frequency.
 *
 * @param[in] pFunction  Pointer to function that should be called at the
 *                       given frequency.
 * @param[in] argument   Argument to be given to the function.
 * @param[in] frequency  Frequency at which to call function at.
 *
 * @return  0 for successful or
 *         -1 if the requested frequency does not match the RTC frequency.
 *****************************************************************************/
int RtcIntCallbackRegister (void(*pFunction)(void*),
                            void* argument,
                            unsigned int frequency)
{
  /* Verify that the requested frequency is the same as the RTC frequency.*/
  if (RTC_CALLBACK_FREQUENCY != frequency)
    return -1;

  rtcCallback    = pFunction;
  rtcCallbackArg = argument;

  return 0;
}

/**************************************************************************//**
 * @brief   This interrupt is triggered every second by the RTC.
 *****************************************************************************/
void RTC_IRQHandler(void)
{
  RTC_IntClear(RTC_IF_COMP0);

  /* Execute callback function if registered. */
  if (rtcCallback && displayEnabled)
    (*rtcCallback)(rtcCallbackArg);

  if (RTC_CALLBACK_FREQUENCY == ++rtcIrqCount)
  {
    /* One second reached, reset irqCount and increment second counter. */
    rtcIrqCount = 0;
    
    seconds++;
  }
}


/**************************************************************************//**
 * @brief   Select a clock source for HF clock, optionally disable other clocks.
 *
 * @param[in] hfClockSelect      The HF clock to select.
 * @param[in] clockDisableMask   Bit masks with clocks to disable.
 *****************************************************************************/
static void SelectClock( CMU_Select_TypeDef hfClockSelect,
                          uint32_t clockDisableMask )
{
  /* Select HF clock. */
  CMU_ClockSelectSet( cmuClock_HF, hfClockSelect );

  /* Disable unwanted clocks. */
  CMU->OSCENCMD     = clockDisableMask;

  /* Turn off clock enables. */
  CMU->HFPERCLKEN0  = 0x00000000;
  CMU->HFCORECLKEN0 = 0x00000000;
  CMU->LFACLKEN0    = 0x00000000;
  CMU->LFBCLKEN0    = 0x00000000;
  CMU->LFCLKSEL     = 0x00000000;
}
