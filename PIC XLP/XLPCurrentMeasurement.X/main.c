/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition                  */

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp              */

asm(".include \"p24F16KA102.inc\"");

/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/

/* i.e. uint16_t <variable_name>; */

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

int main()
{

    /* Configure the oscillator for the device */
//    ConfigureOscillator();

    /* Initialize IO ports and peripherals */
//    InitApp();

    // Activate low-power mode
    
    // Set variable to enable deep-sleep mode
    DSCONbits.DSEN = 1;
    // Transition to the mode
    asm("PWRSAV #SLEEP_MODE");

    while(1) {}
}
