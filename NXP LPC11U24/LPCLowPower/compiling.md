= How to compile code for the mbed NXP LPC11U24 =

The current setup uses make and GCC for ARM embedded.

The compiler and related tools must be installed from https://launchpad.net/gcc-arm-embedded .




== Alternatives ==

The mbed project website (mbed.org) provides a web-based development environment that can be used to compile code.