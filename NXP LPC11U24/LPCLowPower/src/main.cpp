#include <mbed.h>

int main() {

	mbed::DigitalOut led(LED1);

	for(int i = 0; i < 5; i++) {
		led = 1;
		wait(0.2);
		led = 0;
		wait(0.2);
	}
	
	// Deep sleep mode
	deepsleep();

	return 0;
}




